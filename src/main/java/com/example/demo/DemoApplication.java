package com.example.demo;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@SpringBootApplication
public class DemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }

  private final RestTemplate restTemplate = new RestTemplate();

  @GetMapping("/test/{api}")
  public SomeObject test(@PathVariable("api") String api) {
    String input = "{\"value\":\"ok\"}";
    
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<String> entity = new HttpEntity<>(input, headers);
    
    HttpMethod method = "get".equals(api) ? HttpMethod.GET : HttpMethod.POST;
    String url = "http://localhost:8080/" + api;
    return restTemplate.exchange(url, method, entity, SomeObject.class).getBody();
  }
  
  @GetMapping("/get")
  public SomeObject getTestApi(@RequestBody(required = false) SomeObject someObject) {
    return new SomeObject() {{ setValue(someObject != null ? "ok" : "error"); }};
  }

  @PostMapping("/post")
  public SomeObject postTestApi(@RequestBody(required = false) SomeObject someObject) {
    return new SomeObject() {{ setValue(someObject != null ? "ok" : "error"); }};
  }

  @Data
  public static class SomeObject {
    private String value;
  }
  
}
